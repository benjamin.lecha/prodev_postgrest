CREATE TABLE "type_reseau" (
    "id_type_reseau" serial unique,
    "nom_type_reseau" character varying(255)
);

INSERT INTO "type_reseau" ("id_type_reseau", "nom_type_reseau") VALUES
(1,	'Régional'),
(2,	'National');

CREATE TABLE "reseau" (
    "id_reseau" serial,
    "nom_reseau" character varying(255),
    "acronyme_reseau"character varying(20),
    "id_type_reseau" integer NOT NULL,
    CONSTRAINT "type_reseau_id_type_reseau_fkey" FOREIGN KEY (id_type_reseau) REFERENCES type_reseau(id_type_reseau)
);

INSERT INTO "reseau" ("id_reseau", "nom_reseau", "acronyme_reseau", "id_type_reseau") VALUES
(1,	'Réseau Provençal des Informaticiens Développeurs d''Applications',	'ProDev',	1),
(2,	'Métiers de l''Informatique Réunis en Réseau Inter-Etablissement du Nord',	'Min2RIEN',	1),
(3,	'Réseau des acteurs du Développement LOGiciel au sein de l''Enseignement Supérieur et de la Recherche',	'DevLog',	2);

create role auth_user noinherit login password 'test';
grant usage on schema public to auth_user;
grant select on table "reseau" to auth_user;

create role sudo_user noinherit login password 'sudo_test';
grant usage on schema public to sudo_user;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO sudo_user;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO sudo_user;