# ProDev_PostgREST

## Introduction

Ce projet est un support à la présentation pour le réseau ProDev dans le cadre de l'heure ProDev sur PostgREST.

## Pour essayer 

```
docker compose up
```

Ensuite il faut déployer la BDD, je vous recommande Adminer acessible sur : http://localhost:8080

Pour tester l'API, je vous recommande Bruno, des exemples avec cet outil sont fournies.

## Liens et outils utiles

- [PostgREST](https://postgrest.org/en/v12/)
- [PostgreSQL](https://www.postgresql.org/)
- [Bruno](https://www.usebruno.com/)
- [JWT](https://jwt.io/)
- [Docker](https://www.docker.com/)